
#ifndef TestFmaxhaz_hh
#define TestFmaxhaz_hh 1

#include "TObject.h"
#include "InclAblaTest.hh"

class TestFmaxhaz : public InclAblaTest {

public:
  TestFmaxhaz();
  ~TestFmaxhaz();

  void runMe();

private:

  ClassDef(TestFmaxhaz, 0) // Add documentation string here
};

#endif
