
#ifndef TestLpoly_hh
#define TestLpoly_hh 1

#include "TObject.h"
#include "InclAblaTest.hh"

class TestLpoly : public InclAblaTest {

public:
  TestLpoly();
  ~TestLpoly();

  void runMe();

private:

  ClassDef(TestLpoly, 0) // Add documentation string here
};

#endif
