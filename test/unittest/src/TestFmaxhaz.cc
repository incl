// Author: Pekka Kaitaniemi <mailto:pekka.kaitaniemi@helsinki.fi>

#include "TestFmaxhaz.hh"

#include "InclAblaTestDifferencePlotter.hh"

#include "TH1D.h"
#include "TCanvas.h"
#include "TGraph.h"

//#include "InclAblaFunctions.hh"
#include "functionwrapper.hh"
#include "commonwrapper.hh"

#ifndef __CINT__
#include "G4Incl.hh"
#include "G4InclDataDefs.hh"
#endif

#include "Riostream.h"

ClassImp(TestFmaxhaz)

  ///////////////////////////////////////////////////////////////////////////////////
  /* BEGIN_HTML
     <h1>A test generated using template</h1>

     <p>
     This is a test which has been generated using a template.
     </p>
     <p>
     <h3>Comparison between C++ and FORTRAN implementations</h3>

     <img src="plots/TestFmaxhaz.png"/>
     </p>
     END_HTML */
  
TestFmaxhaz::TestFmaxhaz()
{
  setUnitName("UnitName");
  setOriginalUnitName("UnitName");
  setPlotFileName("htmldoc/plots/TestFmaxhaz.png");
  setLogFileName("htmldoc/logs/TestFmaxhaz.txt");
  setLinesOfCode(8);
  setTestStatus(false);
}

TestFmaxhaz::~TestFmaxhaz()
{

}

void TestFmaxhaz::runMe()
{
  // This handles the actual testing.
	
  TCanvas *c1 = new TCanvas();

  c1->SaveAs(getPlotFileName());
  
  setTestStatus(true);
}
