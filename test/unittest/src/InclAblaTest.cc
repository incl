//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: InclAblaTest.cc,v 1.2 2007/09/11 13:28:42 miheikki Exp $ 
// Translation of INCL4.2/ABLA V3 
// Pekka Kaitaniemi, HIP (translation)
// Christelle Schmidt, IPNL (fission code)
// Alain Boudard, CEA (contact person INCL/ABLA)
// Aatos Heikkinen, HIP (project coordination)

// Author: Pekka Kaitaniemi <mailto:pekka.kaitaniemi@helsinki.fi>
#include "InclAblaTest.hh"

#include "Riostream.h"

ClassImp(InclAblaTest)

//////////////////////////////////////////////////////////////////////////////////
/* BEGIN_HTML
<h1>Base class for INCL4/ABLA unit tests</h1>

<h2>Overview</h2>

<p>
This is the base class for INCL/ABLA unit test. Every actual implementation
of unit test is derived from this base class. It implements the basic
functionality shared by all tests. This functionality includes accessor
methods to set and get the following properties for each test: name of the
tested unit and names of the plot and log files
</p>

<h2>Abstract interface to testing routines</h2>

<p>
<a href="InclAblaTest.html">InclAblaTest</a> serves as a base class for abstracted testing
interface. The actual method used for testing is called <a
href=#InclAblaTest:runMe>runMe()</a> which each subclass must implement.
</p>

<h2>Examples of usage</h2>

<p>
One example of a test is presented in class <a href="TestRibm.html">TestRibm</a>.
</p>


  END_HTML*/

InclAblaTest::InclAblaTest()
{

}

InclAblaTest::~InclAblaTest()
{

}

TString InclAblaTest::getUnitName()
{
	// Returns the name of the software unit tested by this class.
	return unitName;
}

TString InclAblaTest::getOriginalUnitName()
{
	// Returns the name of the software unit tested by this class.
	return originalUnitName;
}

TString InclAblaTest::getPlotFileName()
{
	// Returns the name of the plot file generated by the test.
	return plotFileName;
}

TString InclAblaTest::getLogFileName()
{
	// Returns the name of the log file generated by the test.
	return logFileName;
}

void InclAblaTest::writeTestLogFile()
{
  ofstream testLog(getLogFileName());
  testLog << "Test name: " << GetName() << std::endl;
  testLog << "Unit name: " << getUnitName() << std::endl;
  testLog << "Error marginal: " << getErrorMarginal() << std::endl;
  testLog << "Error marginal: " << getErrorMarginal() << std::endl;
}

void InclAblaTest::setUnitName(TString name)
{
	// Set the name of the code unit to be tested.
 	unitName = name;
}

void InclAblaTest::setOriginalUnitName(TString name)
{
	// Set the name of the corresponding FORTRAN code unit.
 	originalUnitName = name;
}

void InclAblaTest::setPlotFileName(TString name)
{
	// Set the output plot file.
 	plotFileName = name;
}

void InclAblaTest::setLogFileName(TString name)
{
	// Set the ouput log file.
 	logFileName = name;
}

void InclAblaTest::setLinesOfCode(Int_t lines)
{
  linesOfCode = lines;
}

Int_t InclAblaTest::getLinesOfCode()
{
  return linesOfCode;
}

Bool_t InclAblaTest::getTestStatus()
{
	// Returns the test status (false = fail, true = pass)
	
	return theTestStatus;
}

void InclAblaTest::setTestStatus(Bool_t status)
{
	// Sets the test status (false = fail, true = pass)
	theTestStatus = status;
}

Double_t InclAblaTest::getErrorMarginal()
{
  return theErrorMarginal;
}

void InclAblaTest::setErrorMarginal(Double_t marginal)
{
  theErrorMarginal = marginal;
}
