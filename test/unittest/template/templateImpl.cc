// Author: Pekka Kaitaniemi <mailto:pekka.kaitaniemi@helsinki.fi>

#include "TestTemplate.hh"

#include "InclAblaTestDifferencePlotter.hh"

#include "TH1D.h"
#include "TCanvas.h"
#include "TGraph.h"

//#include "InclAblaFunctions.hh"
#include "functionwrapper.hh"
#include "commonwrapper.hh"

#ifndef __CINT__
#include "G4Incl.hh"
#include "G4InclDataDefs.hh"
#endif

#include "Riostream.h"

ClassImp(TestTemplate)

  ///////////////////////////////////////////////////////////////////////////////////
  /* BEGIN_HTML
     <h1>A test generated using template</h1>

     <p>
     This is a test which has been generated using a template.
     </p>
     <p>
     <h3>Comparison between C++ and FORTRAN implementations</h3>

     <img src="plots/TestTemplate.png"/>
     </p>
     END_HTML */
  
TestTemplate::TestTemplate()
{
  setUnitName("UnitName");
  setOriginalUnitName("UnitName");
  setPlotFileName("htmldoc/plots/TestTemplate.png");
  setLogFileName("htmldoc/logs/TestTemplate.txt");
  setLinesOfCode(8);
  setTestStatus(false);
}

TestTemplate::~TestTemplate()
{

}

void TestTemplate::runMe()
{
  // This handles the actual testing.
	
  TCanvas *c1 = new TCanvas();

  c1->SaveAs(getPlotFileName());
  
  setTestStatus(true);
}
