
#ifndef TestTemplate_hh
#define TestTemplate_hh 1

#include "TObject.h"
#include "InclAblaTest.hh"

class TestTemplate : public InclAblaTest {

public:
  TestTemplate();
  ~TestTemplate();

  void runMe();

private:

  ClassDef(TestTemplate, 0) // Add documentation string here
};

#endif
