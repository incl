#!/bin/sh

export TESTUNIT=$1
export TESTNAME="Test"$TESTUNIT
export DECLFILE="include/"$TESTNAME".hh"
export IMPLFILE="src/"$TESTNAME".cc"

echo -n "New test name: "
echo $TESTNAME

echo -n "Declaration file: "
echo $DECLFILE

echo -n "Implementation file: "
echo $IMPLFILE

cat template/templateDecl.hh | sed s/TestTemplate/$TESTNAME/g > $DECLFILE
cat template/templateImpl.cc | sed s/TestTemplate/$TESTNAME/g > $IMPLFILE

echo "Done."
echo "Do not forget to add this test to:"
echo "1: Makefile"
echo "2: include/TestingFrameworkLinkDef.h"


